import java.util.Scanner;


public class Problem3 {
    public static void main(String[] args) {
        int num1;
        int num2;
        Scanner kbd = new Scanner(System.in);
        System.out.println("Insert num1");
        num1 = kbd.nextInt();
        System.out.println("Insert num2");
        num2 = kbd.nextInt();
        if (num1 <= num2)
            for (int a = num1; a <=num2; a ++){
                System.out.print(a + " ");
            }
        else 
            System.out.println("Error");
        kbd.close();

        //Scanner sn = new Scanner(System.in);

    }
}
/*เขียนโปรแกรมรับตัวเลข 2 ตัว โดยที่ตัวแรกจะต้องน้อยกว่าหรือเท่ากับตัวหลังเสมอ หากไม่ตรงตามเงื่อนไขจะพิมพ์ว่า Error หากตรงตามเงื่อนไขจะพิมพ์ตัวเลขที่อยู่ระหว่างตัวเลขทั้งสองออกมา Problem3
Please input first number: 2
Please input second number: 5
2 3 4 5
Please input first number: 5
Please input second number: 2
Error
Please input first number: 2
Please input second number: 2
2
 */