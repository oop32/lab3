import java.util.Scanner;


public class Problem5 {
    public static void main(String[] args){
    
        Scanner sc = new Scanner(System.in);
        String str;
        while (true){
            System.out.println("please input something");
            str = sc.nextLine();
            
            if (str.trim().equalsIgnoreCase("bye")) {
                System.out.println("Exit Program");
                break;
            }
            System.out.println(str);
        }
        sc.close();
    }
}
