import java.util.Scanner;

public class Problem9for {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int i = 1;
        System.out.println("please input n: ");
        int n = sc.nextInt();
            while (i <= n){
                int j = 1 ;
                while (j <= n) {
                    System.out.print(j);
                    j++;
                }
                System.out.println();
                i++;
            }
            sc.close();
    }
    }


/* เขียนโปรแกรมเพื่อพิมม์ตัวเลขเป็นรูปสี่เหลี่ยมขนาด nxn เริ่มจาก 1 (ลองทำทั้ง for และ while) Problem9For, Problem9While
Please input n: 3
123
123
123
*/