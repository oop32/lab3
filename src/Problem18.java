import java.util.Scanner;

public class Problem18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num;
        while (true){
            System.out.println("Please select star type 1-4,5 is Exit");
            num = sc.nextInt();
            
            if (num== 1) {
                System.out.print("Please input number: ");
                int n = sc.nextInt();
                for(int i = 0; i < n; i++){
                    for (int j = 0; j < i+1 ; j++){
                        System.out.print("*");
                    }
                    System.out.println();
                }
            }else if (num == 2) {
                System.out.print("Please input number: ");
                int l = sc.nextInt();
                for(int i = l; i > 0; i --){
                    for (int j = 0; j < i ; j ++){
                        System.out.print("*");
                    }
                    System.out.println();
                }
            }else if (num == 3) {
                System.out.print("Please input number: ");
                int n3 = sc.nextInt();
                for(int i = 0; i < n3; i++){
                for (int j = 0; j < i ; j ++){
                System.out.print(" ");
                }
                for (int j = 0; j < n3 - i; j++) {
                System.out.print("*");
                }
                System.out.println();
                }
            
            }else if(num == 4){
                System.out.print("Please input number: ");
                int n4 = sc.nextInt();
                for(int i = n4; i >= 0 ; i--){
                    for(int j=0 ; j < n4 ; j++){
                        if(j>=i) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                        
                    }
                    System.out.println();
                }
            }else if(num == 5){
                System.out.println("Bye Bye!!!!");
                sc.close();
                break;
            }else{
                System.out.println("Error: Please input number between 1-5");
            }
        
                    
            
        }
       
    }
}